'use strict';

describe('Controller: JavaDetailCtrl', function () {

    // load the controller's module
    beforeEach(module('proyectoFullstackApp'));

    var $q,
        Estudiante,
        selected,
        scope,
        vm;

    // Initialize the controller and a mock scope
    beforeEach(inject(function ($controller, _$q_, $rootScope, _Estudiante_) {
        $q = _$q_;
        scope = $rootScope.$new();
        Estudiante = _Estudiante_;
        selected = $q.defer();

        spyOn(Estudiante, 'get').andReturn(selected.promise);

        vm = $controller('JavaDetailCtrl', {
            $scope: scope,
            Estudiante: Estudiante
        });
    }));

    it('should activate a promise', function () {
        expect(vm.promise).not.toBeUndefined();
        expect(vm.promise).not.toBeNull();
    });

    it('should invoke Estudiante.get() function', function () {
        expect(Estudiante.get).toHaveBeenCalled();
    });

    it('should resolve student data', function () {
        selected.resolve({
            "id": 22,
            "nombre": "Lourdes",
            "apellido": "Fernandez",
            "fechaNacimiento": "1996-05-11T00:00:00-04:00"
        });
        scope.$apply();
        expect(vm.selected).not.toBeUndefined();
        expect(vm.selected.id).toBeGreaterThan(0);
        expect(vm.selected.id).toEqual(22);
        expect(vm.selected.nombre).toEqual('Lourdes');
        expect(vm.selected.apellido).toEqual('Fernandez');
    });

    it('should initialize date Object for datePicker', function () {
        selected.resolve({
            "id": 22,
            "nombre": "Lourdes",
            "apellido": "Fernandez",
            "fechaNacimiento": "1996-05-11T00:00:00-04:00"
        });
        scope.$apply();
        expect(vm.selected.fecha).toEqual(jasmine.any(Date));
    });

    it('should change status of date selector', function () {
        expect(vm.isDateOpened).toBeFalsy();
        vm.openDateSelector();
        expect(vm.isDateOpened).toBeTruthy();
    });

    it('should set as invalid model a null value', function () {
        vm.selected = null;
        expect(vm.isValidModel()).toBeFalsy();
    });

    it('should set as invalid model empty values', function () {
        vm.selected = {
            nombre: 'Jose',
            apellido: ''
        };
        expect(vm.isValidModel()).toBeFalsy();

        vm.selected = {
            nombre: '',
            apellido: 'Perez'
        };
        expect(vm.isValidModel()).toBeFalsy();
    });

    it('should set as valid model non empty values', function () {
        vm.selected = {
            nombre: 'Jose',
            apellido: 'Perez'
        };
        expect(vm.isValidModel()).toBeTruthy();
    });
});
