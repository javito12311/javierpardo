'use strict';

describe('Controller: BCtrl', function () {

  // load the controller's module
  beforeEach(module('proyectoFullstackApp'));

  var BCtrl, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    BCtrl = $controller('BCtrl', {
      $scope: scope
    });
  }));

  it('should ...', function () {
    expect(1).toEqual(1);
  });
});
