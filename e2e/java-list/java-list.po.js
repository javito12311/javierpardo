'use strict';

var JavaListPage = function () {

    this.title = element(by.css());
    this.lista = element.all(by.repeater('inscrito in java.inscritos'));

    this.getSizeList = function() {
        return this.lista.count();
    };
};

module.exports = new JavaListPage();
