'use strict';

describe('Java List Page', function() {
    var page;

    beforeEach(function() {
        browser.get('/java');
        page = require('./java-list.po');
    });

    it('should display "Lista de inscritos por Curso" as title', function() {
        expect(page.lista).toBeDefined();
        expect(page.lista).not.toBeNull();
        expect(page.getSizeList()).toBeGreaterThan(0);
    });

    it('should display two rows', function() {
        expect(page.getSizeList()).toBe(2);
    });
});
