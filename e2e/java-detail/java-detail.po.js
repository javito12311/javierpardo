'use strict';

var JavaDetailPage = function () {

    this.nombre = element(by.model('javaDetail.selected.nombre'));
    this.apellido = element(by.model('javaDetail.selected.apellido'));
    this.fecha = element(by.model('javaDetail.selected.fecha'));

    this.guardar = element(by.css('[ng-click="javaDetail.saveChanges()"]'));

    this.getText = function(element) {
       return element.getAttribute('value');
    };
};

module.exports = new JavaDetailPage();
