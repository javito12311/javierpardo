'use strict';

describe('Java Detail Page', function() {
    var page;

    beforeEach(function() {
        browser.get('/java/detail?id=21');
        page = require('./java-detail.po');
    });

    it('should display input fields', function() {
        expect(page.nombre.isDisplayed()).toBeTruthy();
        expect(page.apellido.isDisplayed()).toBeTruthy();
        expect(page.fecha.isDisplayed()).toBeTruthy();
    });

    it('should disable "Guardar" button for empty inputs', function() {
        expect(page.guardar.isEnabled()).toBeTruthy();

        page.nombre.clear();
        page.apellido.clear();
        page.fecha.clear();

        expect(page.guardar.isEnabled()).toBeFalsy();
    });

    it('should allow input new values', function() {
        expect(page.nombre.isEnabled()).toBeTruthy();
        expect(page.apellido.isEnabled()).toBeTruthy();
        expect(page.fecha.isEnabled()).toBeTruthy();

        page.nombre.clear();
        page.apellido.clear();
        page.fecha.clear();

        page.nombre.sendKeys('Mariah');
        page.apellido.sendKeys('Rodrigues');
        page.fecha.sendKeys('1995-06-02');

        expect(page.getText(page.nombre)).toBe('Mariah');
        expect(page.getText(page.apellido)).toBe('Rodrigues');
        expect(page.getText(page.fecha)).toBe('1995-06-02');
    });

    it('should allow save new values', function() {
        page.nombre.clear();
        page.apellido.clear();
        page.fecha.clear();

        page.nombre.sendKeys('Mariah');
        page.apellido.sendKeys('Rodrigues');
        page.fecha.sendKeys('1995-06-02');

        expect(page.guardar.isEnabled()).toBeTruthy();

        page.guardar.click();

        expect(page.getText(page.nombre)).toBe('Mariah');
        expect(page.getText(page.apellido)).toBe('Rodrigues');
        expect(page.getText(page.fecha)).toBe('1995-06-02');
    });
});
